/************************************************************
This function block monitors the health status of connected
CFast cards and outputs the status to the Logger.

Compatible with Swissbit Life Time Monitoring 1.6.8

----------------------------------------------------------

Created by: Yashvin Vedanaparti
Date:		1-15-2020
COPYRIGHT B&R Automation

************************************************************/

/**********************************************************
Error Codes

- -536805275: File length = 0 / empty file.
- 20700-20799: FileIO error.
**********************************************************/

#include <bur/plctypes.h>


#ifdef __cplusplus
	extern "C"
	{
#endif
#include "cfhealth.h"
	
#ifdef __cplusplus
	};
#endif

/************************************************************
Parse function obtains next word from buff string. Ignores
white space.
************************************************************/
STRING * ParseNextWord(struct CFastMonitor* inst)
{	
	//initialize parse vars
	brsmemset(inst->Internal.curWord, 0, brsstrlen(inst->Internal.curWord));
	inst->Internal.curWordIndex = 0;
	inst->Internal.curWordLen = 1;
	
	while(1)
	{
		inst->Internal.curChar = inst->Internal.buff[inst->Internal.parseIndex];
		inst->Internal.parseIndex++;
		
		//End of file reached if word length = 1 and parseIndex is at last char
		if (inst->Internal.parseIndex == inst->Internal.File.FileLen)
		{
			return "EOF!";
		}
			//If space char encountered and wordLen = 1, do nothing
		else if ((inst->Internal.curChar == ' ' || 
			inst->Internal.curChar == '\r' || 
			inst->Internal.curChar == '\n' || 
			inst->Internal.curChar == '\t') && 
			(inst->Internal.curWordLen == 1))
		{
			
		}
			//If space char encountered and wordLen > 1, end of word reached so return
		else if ((inst->Internal.curChar == ' ' || 
			inst->Internal.curChar == '\r' || 
			inst->Internal.curChar == '\n' || 
			inst->Internal.curChar == '\t') && 
			(inst->Internal.curWordLen > 1))
		{
			inst->Internal.curWord[inst->Internal.curWordIndex] = '\0';
			return inst->Internal.curWord;
		}
			//if non-space char encountered, append curWord with curChar
		else {
			inst->Internal.curWord[inst->Internal.curWordIndex++] = inst->Internal.curChar;
			inst->Internal.curWordLen++;
		}
	}

}

/************************************************************
Looks for the logbook with specified name. If it does not
exist, it will create it and return its Ident.
************************************************************/

ArEventLogIdentType * GetLogIdent(struct CFastMonitor *inst, STRING * logBookName)
{
	inst->Internal.LogBook.GetEventLogIdent.Execute = 1;
	brsstrcpy(inst->Internal.LogBook.GetEventLogIdent.Name, logBookName);
	ArEventLogGetIdent(&inst->Internal.LogBook.GetEventLogIdent);
	
	if (inst->Internal.LogBook.GetEventLogIdent.Error)
	{
		//If Logbook not found, create it
		if (inst->Internal.LogBook.GetEventLogIdent.StatusID == arEVENTLOG_ERR_LOGBOOK_NOT_FOUND)
		{
			inst->Internal.LogBook.CreateEventLog.Execute = 1;
			brsstrcpy(inst->Internal.LogBook.CreateEventLog.Name, logBookName);
			inst->Internal.LogBook.CreateEventLog.Size = 4096;
			inst->Internal.LogBook.CreateEventLog.Persistence = arEVENTLOG_PERSISTENCE_REMANENT;
				
			if (inst->Internal.LogBook.CreateEventLog.Done)
			{
				return inst->Internal.LogBook.CreateEventLog.Ident;
			}
			//Call the FuB only if it's not Done AND not Busy
			else if (inst->Internal.LogBook.CreateEventLog.Busy == 0)
			{
				ArEventLogCreate(&inst->Internal.LogBook.CreateEventLog);
			}	
		}
	}
	//If GetIdent returns an Ident, then logbook exists, so return it's ident
	else if (inst->Internal.LogBook.GetEventLogIdent.Done)
	{
		return inst->Internal.LogBook.GetEventLogIdent.Ident;
	}


}

/************************************************************
Writes an event message to the logbook. If successfully
written, returns a 1. If an error occurs, returns 0.
************************************************************/

BOOL WriteEventToLog(struct CFastMonitor *inst, DINT eventID)
{
	
	//Get Logbook ident
	inst->Internal.LogBook.WriteEventLog.Ident 			= GetLogIdent(inst, "cfastlog");
	inst->Internal.LogBook.WriteEventLog.EventID 		= eventID;
	inst->Internal.LogBook.WriteEventLog.AddDataSize 	= 160;
	inst->Internal.LogBook.WriteEventLog.AddDataFormat 	= arEVENTLOG_ADDFORMAT_TEXT; //Series of null-terminated strings
	inst->Internal.LogBook.WriteEventLog.AddData 		= &inst->Internal.LogBook.logMessage;
	inst->Internal.LogBook.WriteEventLog.Execute 		= 1;
	
	
	ArEventLogWrite(&inst->Internal.LogBook.WriteEventLog);
	
	
	if (inst->Internal.LogBook.WriteEventLog.Done)
	{
		//Call FuB again to reset execute and other values. Otherwise, FuB never sees Enable reset and will not write to Logger 2nd time
		inst->Internal.LogBook.WriteEventLog.Execute = 0;
		ArEventLogWrite(&inst->Internal.LogBook.WriteEventLog);		
		return 1;
	}
	else if(inst->Internal.LogBook.WriteEventLog.Error)
	{
		inst->Internal.LogBook.WriteEventLog.Execute = 0;
		ArEventLogWrite(&inst->Internal.LogBook.WriteEventLog);
		return 0;
	}
	
	return 0;

}



/************************************************************
Main method
************************************************************/
void CFastMonitor(struct CFastMonitor* inst)
{
	//Frequency Timer -----------------------------------
	
	inst->Internal.FreqTimer_TON.PT = inst->FreqTime;
	
	if (inst->Enable)
	{
		inst->Internal.FreqTimer_TON.IN = 1;
	}
	
	//This condition signifies the operation has completed and timer expired, so FuB
	//will execute again
	if (inst->Internal.FreqTimer_TON.Q && inst->Error == 0 && inst->CommandBusy == 0)
	{
		inst->Internal.state = CFHEALTH_STATE_INIT;
		inst->Internal.Run = 1;
		inst->Internal.FreqTimer_TON.IN = 0;
	}
	//-------------------------------------------------

	if (inst->Enable == 0 && inst->Error == 0)
	{
		inst->Internal.Run = 0;
		if (inst->CommandBusy == 0)
		{
			inst->Internal.state = CFHEALTH_STATE_INIT;
		}
	}
	
	switch (inst->Internal.state)
	{
		case CFHEALTH_STATE_INIT:
			
			inst->CommandBusy 			= 0;
			inst->CommandDone			= 0;
			inst->Internal.parseIndex 	= 0;
			inst->Internal.curWordIndex = 0;
			inst->Internal.curWordLen 	= 1;
			inst->Internal.cardIndex	= 0;

			brsmemset(inst->Internal.curWord, 0, brsstrlen(inst->Internal.curWord));
			brsmemset(inst->Internal.parsedWord, 0, brsstrlen(inst->Internal.parsedWord));
			//TODO check if brsmemset Internal to 0
			
			if (inst->Internal.Run)
			{
				inst->Internal.state 	= CFHEALTH_STATE_LINK_FILE_DEVICE;
				inst->CommandBusy 		= 1;
			}
			
			break;
		
		case CFHEALTH_STATE_LINK_FILE_DEVICE:
			inst->Internal.File.DevLink_0.enable 	= 1;
			inst->Internal.File.DevLink_0.pDevice 	= (UDINT) CFHEALTH_DEVICE_NAME;
			inst->Internal.File.DevLink_0.pParam 	= inst->pDevParam;
			
				
			DevLink(&inst->Internal.File.DevLink_0);
				
				
			if (inst->Internal.File.DevLink_0.status == 0)
			{
				//Executed cleanly, move to next state
				inst->Internal.state = CFHEALTH_STATE_FILE_OPEN;
				inst->Internal.File.DevLink_0.enable = 0;
			}
			else if (inst->Internal.File.DevLink_0.status != ERR_FUB_BUSY)
			{
				//If status not complete and not busy, then in error
				inst->Status 							= inst->Internal.File.DevLink_0.status;
				inst->Internal.File.DevLink_0.enable 	= 0;
				inst->Internal.state 					= CFHEALTH_STATE_ERROR;
				inst->Internal.PrevState 				= CFHEALTH_STATE_LINK_FILE_DEVICE;
			}
			
			
			break;
		
		case CFHEALTH_STATE_FILE_OPEN:
        	
			inst->Internal.File.Open.enable 	= 1;
			inst->Internal.File.Open.pDevice 	= (UDINT) CFHEALTH_DEVICE_NAME;
			inst->Internal.File.Open.pFile 		= (UDINT) CFHEALTH_FILE_NAME;
			inst->Internal.File.Open.mode 		= fiREAD_ONLY;
			inst->Internal.File.FileIdent		= inst->Internal.File.Open.ident;	
			FileOpen(&inst->Internal.File.Open);
			
		
			if(inst->Internal.File.Open.status == 0)
			{
				//File opened but empty
				if (inst->Internal.File.Open.filelen == 0)
				{
					inst->Internal.state 			= CFHEALTH_STATE_ERROR;
					inst->Internal.PrevState 		= CFHEALTH_STATE_FILE_OPEN;
					inst->Internal.File.Open.enable = 0;
					inst->Status 					= CFHEALTH_ERR_FILE_EMPTY;	//File empty
				}
					//Executed cleanly, move to next state
				else
				{
					inst->Internal.state 			= CFHEALTH_STATE_READ_TO_STRING;
					inst->Internal.File.Open.enable = 0;	
					inst->Internal.File.FileLen 	= inst->Internal.File.Open.filelen;
				}
			}
			else if (inst->Internal.File.Open.status != ERR_FUB_BUSY) 
			{
				//If status not complete and not busy, then in error
				inst->Internal.state 				= CFHEALTH_STATE_ERROR;
				inst->Internal.PrevState 			= CFHEALTH_STATE_FILE_OPEN;
				inst->Internal.File.Open.enable 	= 0;
				inst->Status 						= inst->Internal.File.Open.status;
			}
				
			break;
		
		case CFHEALTH_STATE_READ_TO_STRING:
			
			inst->Internal.File.Read.enable 		= 1;
			inst->Internal.File.Read.ident 			= inst->Internal.File.FileIdent;
			inst->Internal.File.Read.offset 		= 0;
			inst->Internal.File.Read.len 			= inst->Internal.File.FileLen;
			inst->Internal.File.Read.pDest 			= &inst->Internal.buff[0];
			
			
			//Read entire file to buff string
			FileRead(&inst->Internal.File.Read);
			
			if (inst->Internal.File.Read.status == 0)
			{
				inst->Internal.state = CFHEALTH_STATE_PARSE_MODEL_EOF;
				inst->Internal.File.Read.enable = 0;
				inst->Internal.buff[inst->Internal.File.FileLen] = '\0';
			}
			else if (inst->Internal.File.Read.status != ERR_FUB_BUSY)
			{
				//If status not complete and not busy, then in error
				inst->Internal.state 			= CFHEALTH_STATE_ERROR;
				inst->Internal.PrevState 		= CFHEALTH_STATE_READ_TO_STRING;
				inst->Internal.File.Read.enable = 0;
				inst->Status 					= inst->Internal.File.Read.status;
			}
			
			break;
		
		/*Parses through buff[] for "Model:". Once Model number is found, the following
		info will be for the card pertaining to that model. After entry is logged,
		control is returned to this state to parse for "Model:" again. If found,
		then system has 2 cards and therefore will loop through again. If EOF reached,
		then entire file has been parsed and the state changes to FILE_CLOSE. */
		case CFHEALTH_STATE_PARSE_MODEL_EOF:
			//Parse file until model number found
			brsstrcpy(inst->Internal.parsedWord, ParseNextWord(inst));
			if (brsstrcmp(inst->Internal.parsedWord, "Model:") == 0)
			{
				brsstrcpy(inst->Cards[inst->Internal.cardIndex].ModelNumber, ParseNextWord(inst));
				inst->Internal.state = CFHEALTH_STATE_PARSE_SERIAL;
			}
				//If ParseNextWord returns "EOF", En of file reached so close file
			else if (brsstrcmp(inst->Internal.parsedWord,"EOF!") == 0)
			{
				inst->Internal.state 	= CFHEALTH_STATE_FILE_CLOSE;
			}
			
			break;
		
		case CFHEALTH_STATE_PARSE_SERIAL:
			
			//Parse file until serial number found
			brsstrcpy(inst->Internal.parsedWord, ParseNextWord(inst));
			if (brsstrcmp(inst->Internal.parsedWord, "Serial:") == 0)
			{
				brsstrcpy(inst->Cards[inst->Internal.cardIndex].SerialNumber, ParseNextWord(inst));
				inst->Internal.state = CFHEALTH_STATE_PARSE_STATUS;
			}
			
			break;
		
		case CFHEALTH_STATE_PARSE_STATUS:
			
			brsstrcpy(inst->Internal.parsedWord, ParseNextWord(inst));
			if (brsstrcmp(inst->Internal.parsedWord,"PASS") == 0)
			{
				inst->Cards[inst->Internal.cardIndex].CardFailure = 0;
				inst->Internal.state = CFHEALTH_STATE_PARSE_ERASE_LIFE;
			}
			else if (brsstrcmp(inst->Internal.parsedWord, "FAIL") == 0 || brsstrcmp(inst->Internal.parsedWord, "WARNING") == 0)
			{
				inst->Cards[inst->Internal.cardIndex].CardFailure = 1;
				inst->Internal.state = CFHEALTH_STATE_PARSE_ERASE_LIFE;
			}
			
			break;
		
		case CFHEALTH_STATE_PARSE_ERASE_LIFE:
			
			brsstrcpy(inst->Internal.parsedWord, ParseNextWord(inst));
			if (brsstrcmp(inst->Internal.parsedWord, "Remaining")==0)
			{
				if (brsstrcmp(ParseNextWord(inst), "erase")==0)
				{
					//call ParseNextWord twice to ignore "life" and "time" in "Remaining erase life time:" in cfast_status_list file 
					ParseNextWord(inst);
					ParseNextWord(inst);
					//Parse for percent as a string, and convert to DINT value. Percent sign is ignored using brsatoi
					brsstrcpy(inst->Cards[inst->Internal.cardIndex].RemainingEraseLifeStr, ParseNextWord(inst));
					
					inst->Internal.state = CFHEALTH_STATE_LOG_ENTRY;
				}
			}
			
			break;
		
		case CFHEALTH_STATE_LOG_ENTRY:
			//Format logger message to print in ASCII Data column
			brsmemset(inst->Internal.LogBook.logMessage, 0, brsstrlen(inst->Internal.LogBook.logMessage));
				
			//SnPrintf
			snprintf2(inst->Internal.LogBook.logMessage, brsstrlen(inst->Internal.LogBook.logMessage), 
			"%s: Remaining erase life time: %s", inst->Cards[inst->Internal.cardIndex].ModelNumber, 
			inst->Cards[inst->Internal.cardIndex].RemainingEraseLifeStr);
			//
			
			if (inst->Cards[inst->Internal.cardIndex].CardFailure)
			{
				WriteEventToLog(inst, CFHEALTH_WARNING);
			}
			else
			{
				WriteEventToLog(inst, CFHEALTH_INFO);
			}
			
			inst->Internal.cardIndex++;
			inst->Internal.state = CFHEALTH_STATE_PARSE_MODEL_EOF;
				 
			
			break;
		
		case CFHEALTH_STATE_FILE_CLOSE:
			
			//Close File
			inst->Internal.File.Close.enable = 1;
			inst->Internal.File.Close.ident = inst->Internal.File.FileIdent;
			FileClose(&inst->Internal.File.Close);
			
	
			if(inst->Internal.File.Close.status == 0) 
			{
				inst->Internal.File.Close.enable 	= 0;
				inst->Internal.state 				= CFHEALTH_STATE_UNLINK_DEVICE;
				
			}	
			else if (inst->Internal.File.Close.status != ERR_FUB_BUSY) 
			{
				//If status not complete and not busy, then in error
				inst->Internal.state 				= CFHEALTH_STATE_ERROR;
				inst->Internal.PrevState 			= CFHEALTH_STATE_FILE_CLOSE;
				inst->Internal.File.Close.enable 	= 0;
				inst->Status 						= inst->Internal.File.Close.status;
			}
				
			break;
		
		case CFHEALTH_STATE_UNLINK_DEVICE:
			
			inst->Internal.File.DevUnlink_0.enable = 1;
			inst->Internal.File.DevUnlink_0.handle = inst->Internal.File.DevLink_0.handle;
			
			DevUnlink(&inst->Internal.File.DevUnlink_0);
			
			if (inst->Internal.File.DevUnlink_0.status == 0)
			{
				inst->Internal.File.DevUnlink_0.enable 	= 0;
				inst->Internal.state 					= CFHEALTH_STATE_COMPLETE;
			
			}
			else if (inst->Internal.File.DevUnlink_0.status != ERR_FUB_BUSY)
			{
				//If status not complete and not busy, then in error
				inst->Status = inst->Internal.File.DevUnlink_0.status;
				inst->Internal.state 					= CFHEALTH_STATE_ERROR;
				inst->Internal.PrevState 				= CFHEALTH_STATE_UNLINK_DEVICE;
				inst->Internal.File.DevUnlink_0.enable	= 0;
			}
			
			break;
			
		case CFHEALTH_STATE_COMPLETE:
				
			inst->CommandDone 	= 1;
			inst->CommandBusy 	= 0;
			inst->Internal.Run 	= 0;
				
			break;
			
		case CFHEALTH_STATE_ERROR:
				
			inst->CommandBusy 				= 0;
			inst->CommandDone 				= 0;
			inst->Internal.Run 				= 0;
			inst->Error						= 1;
			
			//Write error to log---------------------------
			brsmemset(inst->Internal.LogBook.logMessage, 0, brsstrlen(inst->Internal.LogBook.logMessage));
			//snprintf
			snprintf2(inst->Internal.LogBook.logMessage, 
				brsstrlen(inst->Internal.LogBook.logMessage), 
				"Status ID: %d", inst->Status);
			//
			if (inst->Internal.ErrorPrinted == 0)
			{
				if (inst->Status == CFHEALTH_ERR_FILE_EMPTY)
				{
					//File is empty
					WriteEventToLog(inst, CFHEALTH_ERR_FILE_EMPTY);
				}
				else
				{
					//File IO error
					WriteEventToLog(inst, CFHEALTH_ERR_FILE_IO);
				}
				inst->Internal.ErrorPrinted = 1;
			}
			
			//----------------------------------------------
			if (inst->ErrorReset)
			{
				//If Devlink threw the error, then File device needs to be unlinked
				if (inst->Internal.PrevState == CFHEALTH_STATE_LINK_FILE_DEVICE)
				{
					inst->Internal.state = CFHEALTH_STATE_UNLINK_DEVICE;
				}
				//If FileOpen or FileRead threw the error, then file must be closed as well
				else if(inst->Internal.PrevState == CFHEALTH_STATE_FILE_OPEN || inst->Internal.PrevState == CFHEALTH_STATE_READ_TO_STRING)
				{
					inst->Internal.state = CFHEALTH_STATE_FILE_CLOSE;
				}
				//Else, FileIO did not cause error so return to INIT
				else
				{
					inst->Internal.state = CFHEALTH_STATE_INIT;
				}
				inst->CommandBusy 			= 1;
				inst->CommandDone			= 0;
				inst->Error 				= 0;
				inst->Status 				= 0;
				inst->Internal.ErrorPrinted = 0;
			}
			break;
			
	}//End switch
			
	//FuB Call
	TON(&inst->Internal.FreqTimer_TON);
	
	
}

