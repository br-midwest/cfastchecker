
{REDUND_CONTEXT} FUNCTION_BLOCK CFastMonitor (*Monitors health status of connected CFast cards*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		Enable : BOOL; (*Enable and execute the function block*) (* *) (*#PAR*)
		ErrorReset : BOOL; (*Reset active errors command*) (* *) (*#PAR*)
		pDevParam : UDINT; (*Pointer to the Devlink Device parameter string*) (* *) (*#PAR*)
		FreqTime : TIME := T#01d; (*Frequency at which program will execute. Default at 1 day*) (* *) (*#PAR*)
	END_VAR
	VAR_OUTPUT
		Error : BOOL; (*Function block in error*) (* *) (*#PAR*)
		Status : UDINT; (*Error code*) (* *) (*#PAR*)
		CommandDone : BOOL; (*Function block finished executing*) (* *) (*#CMD*)
		CommandBusy : BOOL; (*Function block busy*) (* *) (*#CMD*)
	END_VAR
	VAR
		Cards : ARRAY[0..1] OF CFastCardType; (*CFast card(s) data array*)
		Internal : InternalType := (0); (*Internal variables*)
	END_VAR
END_FUNCTION_BLOCK
