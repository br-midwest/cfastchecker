:: This batch file calls the executable sbltm-cli.exe and writes the output
:: to cfast_status_list.txt
::
:: Yashvin Vedanaparti | 1-21-2020
:: B&R Automation


@ECHO OFF

::executes swissbit cfast monitoring tool and stores output in file "cfast_status_list.txt"
::Specified "human readable format", "no databasing", and write output to file
::File is created on first run
"C:\cfastmonitor\sbltm-cli.exe" /l human --db no > C:\cfastmonitor\cfast_status_list.txt

