On the ARwin or Hypervisor system, create a folder in the C:\ directory named "cfastmonitor", and copy "cfast_monitor.bat" and "sbltm-cli.exe" into it.

These files can be found in the "windows files" folder.

For setup instructions, consult help documentation for this library.

Add the library to an Automation Studio project, select it, and press F1 to load the help documentation.
